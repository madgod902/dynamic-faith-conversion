country_decisions = {

	dfc_modmenu = {
		major = yes
		potential = {
			ai = no
			NOT = { has_country_flag = DFC_help_open }
		}
		allow = {
			always = yes
		}
		effect = {
			hidden_effect = {
				country_event = {
					id = DFC_help.1
					days = 0
				}
			}
		}
	}

	buy_missionaries = {
		potential = {
			NOT = { has_country_flag = bought_missionaries }
		}

		allow = {
			years_of_income = 1
		}

		effect = {
			add_years_of_income = -1
			custom_tooltip = buy_missionaries_tooltip
			hidden_effect = {
				set_country_flag = bought_missionaries
			}
		}
	}

	buy_missionaries_off = {
		potential = {
			has_country_flag = bought_missionaries
		}

		allow = {
			always = yes
		}

		effect = {
			add_years_of_income = 0.5
			custom_tooltip = buy_missionaries_tooltip
			hidden_effect = {
				clr_country_flag = bought_missionaries
			}
		}
	}

}